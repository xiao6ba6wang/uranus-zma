package com.empress.uranus.zma.common.utils;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;

public class MessageFormatUtil {
	
	public static int CalcCrc16_2(char[] pData, int nLength) {
		short cRc_16 = 0;
        short cnCRC_16 = (short) 0x8005;
        long[] cRctable_16 = new long[256];
        int i, j, k;
        for (i=0,k=0;i<256;i++,k++)
        {
            cRc_16 = (short) (i<<8);
            for (j=8;j>0;j--)
            {
                if ((cRc_16&0x8000) != 0)
                    cRc_16 = (short)((cRc_16<<=1)^cnCRC_16);
                else
                    cRc_16<<=1;
            }
            cRctable_16[k] = cRc_16;
        }
        i = 0;
        while (nLength>0)
        {
            cRc_16 = (short)( (cRc_16 << 8) ^ (short)cRctable_16[((cRc_16>>8) ^ pData[i]) & 0xff]);
            nLength--;
            i++;
        }
        return cRc_16;
    }
    public static char[] numberToChars(long number, int length) {
        if (length <= 0) {
            return null;
        }
        if (length > 8) {
            length = 8;
        }
        char[] result = new char[length];
        for (int i = 0; i < length; i++) {
            int shift = i * 8;
            long mask = 0xff << shift;
            result[i] = (char) ((mask & number) >> shift);
        }
        return result;
    }
    public static byte[] longToBytes(long number, int len)
    {
        byte[] destData = new byte[len];

        if (len < 0)
        {
            return destData;
        }
        if (len > 8){
            len = 8;
        }

        for (int i = 0; i < len; i++)
        {
            int offset = i * 8;
            long mask = 0xff << offset;
            destData[i] = (byte)((mask & number) >> offset);
        }

        return destData;
    }

    public static char[] floatToChars(float data) {
        int floatValue = Float.floatToIntBits(data);
        return numberToChars(floatValue, 4);
    }
    
    public static String bytesToString(byte[] data) {
        return new String(data);
    }
    
    public static byte[] subByteArray(byte[] data, int begin, int length) {
        if (data == null || data.length <= 0 || length <= 0 || begin >= data.length) {
            return null;
        }
        if (begin < 0) {
            begin = 0;
        }
        if (length > data.length - begin) {
            length = data.length - begin;
        }
        byte[] subChars = new byte[length];
        System.arraycopy(data, begin, subChars, 0, length);
        return subChars;
    }
    public static int bytesToNumber(byte[] data, int length) {
        int result = 0;
        if (length == 0) {
            return 0;
        }
        if (length > 4) {
            length = 4;
        }
        if (length > data.length) {
            length = data.length;
        }
        /*
        for (int i = length - 1; i >= 0; i--) {
            result = result + ((data[i] + 256) % 256) * (int) (Math.pow(256, i));
        }*/
        for (int i = 0; i <= length - 1; i++) {
            result = result + ((data[i] + 256) % 256) * (int) (Math.pow(256, (length - 1 - i)));
        }
        return result;
    }
    public static char[] stringToChars(String s, int length) {
        char[] result = new char[length];
        int n = 0;
        for (int i = 0; i < result.length-1; i++) {
            try {
                if (i < s.length()) {
                    char tmp = s.charAt(i);
                    if(isChinese(tmp)){
                    	
                		String chineseString = String.valueOf(tmp);
                		byte[] chineseBytes = null;
        				try {
        					chineseBytes = chineseString.getBytes("GBK");
        					if(chineseBytes.length != 2)
        	        		{
        	        			//LOGGER.error("解析基本报文格式时中文字符不等于两个字节，待解析的字符:" + tmp);
        	        		}
        					result[n++] = (char)(chineseBytes[0]);
        					result[n++] = (char)(chineseBytes[1]);
        				} catch (UnsupportedEncodingException e) {
        					//LOGGER.warn("解析基本报文格式时异常，待解析的字符:" + tmp, e);
        					result[n++] = (char)(0x00);
        					result[n++] = (char)(0x00);
        				}
                	}else{
                		result[n++] = s.charAt(i);
                	}
                } else {
                    result[n++] = 0x0;
                }
            } catch (Exception e) {
                break;
            }
        }
        result[length - 1] = 0x0;
        return result;
    }
    //根据字节码判断
    private static boolean isChinese(char c) {
        return c >= 0x4E00 &&  c <= 0x9FA5;
    }
    public static char[] getCharArray(char[][] msg, int length) {
        char[] result = new char[length];
        int index = 0;
        for (char[] it : msg) {
            if (it == null || it.length == 0) {
                continue;
            }
            for (char anIt : it) {
                result[index] = anIt;
                index++;
            }
        }
        return result;
    }
    public static char[] getChars(byte[] bytes) {
        Charset cs = Charset.forName ("UTF-8");
        ByteBuffer bb = ByteBuffer.allocate (bytes.length);
        for(int i=0;i<bytes.length;i++){
        	if(bytes[i] < 0){
        		bytes[i] = (byte) ((bytes[i]) + 0xff + 0x01);
        	}
        }
        bb.put (bytes);
        bb.flip ();
        CharBuffer cb = cs.decode (bb);
        return cb.array();
	}
    public static char[] changeBytesToChars(byte[] bytes, int len) {
        char[] data = new char[len];
        for(int i=0;i<len;i++){
        	data[i] = (char) bytes[i];
        }
        return data;
    }
}
