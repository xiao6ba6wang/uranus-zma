package com.empress.uranus.zma.common.enums;

/**
 * 报文类型
 * 
 * @author taoxuejun
 * @date 2017-12-13
 */
public enum WarehousePacketEnum {

	/**
	 * 机器注册请求
	 */
	REGISTER_REQUEST((long)0x01, "机器注册请求"),
	/**
	 * 机器注册响应
	 */
	REGISTER_RESPONSE((long)0x02, "机器注册响应"),
	/**
	 * 机器运营心跳请求
	 */
	HEARTBEAT_REQUEST((long)0x03, "机器运营心跳请求"),
	/**
	 * 机器运营心跳响应
	 */
	HEARTBEAT_RESPONSE((long)0x04, "机器运营心跳响应"),
	/**
	 * 机器运营心跳间隔设置请求
	 */
	HEARTBEAT_INTERVAL_REQUEST((long)0x05, "机器运营心跳间隔设置请求"),
	/**
	 * 机器运营心跳间隔设置响应
	 */
	HEARTBEAT_INTERVAL_RESPONSE((long)0x06, "机器运营心跳间隔设置响应"),
	/**
	 * 出货请求
	 */
	SHIPMENT_REQUEST((long)0x07, "出货请求"),
	/**
	 * 出货响应
	 */
	SHIPMENT_RESPONSE((long)0x08, "出货响应"),
	/**
	 * 充电请求
	 */
	CHARGE_REQUEST((long)0x09, "充电请求"),
	/**
	 * 充电响应
	 */
	CHARGE_RESPONSE((long)0x0A, "充电响应"),
	/**
	 * 维护请求
	 */
	MAINTAIN_REQUEST((long)0x0B, "维护请求"),
	/**
	 * 维护响应
	 */
	MAINTAIN_RESPONSE((long)0x0C, "维护响应"),
	/**
	 * 电磁锁请求
	 */
	ELECTROMAGNETIC_LOCK_REQUEST((long)0x0D, "电磁锁请求"),
	/**
	 * 电磁锁响应
	 */
	ELECTROMAGNETIC_LOCK_RESPONSE((long)0x0E, "电磁锁响应"),
	/**
	 * 修改虚拟编号请求
	 */
	REVISE_VIRTUAL_REQUEST((long)0x0F, "修改虚拟编号请求"),
	/**
	 * 修改虚拟编号响应
	 */
	REVISE_VIRTUAL_RESPONSE((long)0x10, "修改虚拟编号响应"),
	/**
	 * 出纸反馈请求
	 */
	OUTPAPER_FEEDBACK_REQUEST((long)0x11, "出纸反馈请求"),
	/**
	 * 出纸反馈响应
	 */
	OUTPAPER_FEEDBACK_RESPONSE((long)0x12, "出纸反馈响应");
	/**
	 * 功能码
	 */
	private final Long functionCode;
	/**
	 * 功能码对应的描述
	 */
	private final String functionDescription;
	
	private WarehousePacketEnum(Long functionCode, String functionDescription) {
		this.functionCode = functionCode;
		this.functionDescription = functionDescription;
	}
	
	public Long getFunctionCode() {
		return functionCode;
	}

	public String getFunctionDescription() {
		return functionDescription;
	}

	public static WarehousePacketEnum valueOf(long functionCode) {
		for (WarehousePacketEnum warehousePacket : values()) {
			if (warehousePacket.functionCode == functionCode) {
				return warehousePacket;
			}
		}
		return null;
	}
	
	@Override
	public String toString() {
		return Long.toString(functionCode);
	}
}
