package com.empress.uranus.zma.common.utils;

public class PrinterUtil {

    public static String printHex(byte[] bytes, int len) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < len; i++) {
            sb.append(printHex(bytes[i], i));
        }
        return sb.toString();
    }
    
    private static String printHex(int n, int i){
        String s = Integer.toHexString(n + 256);
        s = s.toUpperCase().substring(s.length() - 2);
        if (i > 0 && (i % 16) == 0) {
            return "\n  " + s;
        }else{
            return "  " + s;
        }
    }
}
