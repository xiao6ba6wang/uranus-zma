package com.empress.uranus.zma.common.constants;

/**
 * 纸巾机服务器常量类
 * 
 * @author taoxuejun
 * @date 2017-12-10
 */
public class MCConstant {

	/**
	 * netty server 的端口号
	 */
	public static final Integer NETTY_SERVER_PORT = 8500;
	/**
	 * 服务启动30秒后运行
	 */
	public static final Integer SCHEDULE_INIT_DELAY = 5*1000;
	/**
	 * 纸巾机服务器启动执行周期1秒
	 */
	public static final Integer SCHEDULE_EXECUTE_PERIOD = 1000;
	/**
	 * 纸巾机终端作为影院客户端的一个模拟部分，为区别其他报文，在13规范上每个报文ID加上了0x70
	 * 如果纸巾机终端是影院客户端模拟的测试环境，在作crc校验时会将每个报文id减去0x70
	 * 在真实环境下，是不需要作这部分操作的
	 */
	public static final boolean MC_ENV_TEST = false;
}