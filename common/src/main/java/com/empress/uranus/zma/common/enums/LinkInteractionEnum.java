package com.empress.uranus.zma.common.enums;

/**
 * 定义数据双方交互
 * 
 * @author taoxuejun
 * @date 2017-12-12
 */
public enum LinkInteractionEnum {

	/**
	 * 纸巾机器客户端向后台服务端发请求
	 */
	ECHO_REQUEST(1, "纸巾机器客户端向后台服务端发请求"),
	/**
	 * 后台服务端向纸巾机器客户端应答
	 */
	ECHO_RESPONSE(2, "后台服务端向纸巾机器客户端应答");
	/**
	 * 数据包交互代号
	 */
	private final Integer packetType;
	/**
	 * 数据包交互代号说明
	 */
	private final String echoPhrase;
	public Integer getPacketType() {
		return packetType;
	}
	public String getEchoPhrase() {
		return echoPhrase;
	}
	private LinkInteractionEnum(Integer packetType, String echoPhrase) {
		this.packetType = packetType;
		this.echoPhrase = echoPhrase;
	}
	public static LinkInteractionEnum valueOf(int packetType) {
		for (LinkInteractionEnum linkInteractionEnum : values()) {
			if (linkInteractionEnum.packetType == packetType) {
				return linkInteractionEnum;
			}
		}
		return null;
	}
	
	@Override
	public String toString() {
		return Integer.toString(packetType);
	}
}
