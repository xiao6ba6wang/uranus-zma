package com.empress.uranus.zma.biz.machine;

import io.netty.channel.ChannelOutboundHandlerAdapter;

/**
 * 纸巾机服务器业务层处理数据输出接口
 * 
 * @author taoxuejun
 * @date 2017-12-10
 */
public abstract class MCInvokeOutboundHandlerBiz extends ChannelOutboundHandlerAdapter {

}
