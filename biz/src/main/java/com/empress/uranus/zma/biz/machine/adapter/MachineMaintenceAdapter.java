package com.empress.uranus.zma.biz.machine.adapter;

import com.empress.uranus.zma.common.enums.LinkInteractionEnum;
import com.empress.uranus.zma.common.enums.WarehousePacketEnum;
import com.empress.uranus.zma.common.utils.MessageFormatUtil;
import com.empress.uranus.zma.domain.machine.dto.request.MachineMaintenceRequestDTO;
import com.empress.uranus.zma.domain.machine.dto.response.MachineMaintenceResponseDTO;
import com.empress.uranus.zma.service.model.WarehousePacketModel;

public class MachineMaintenceAdapter {

	public static MachineMaintenceRequestDTO machineBytesToPacketDTO(byte[] machineBytes){
		
		MachineMaintenceRequestDTO machineMaintenceRequestDTO = new MachineMaintenceRequestDTO();
		Integer maintenceStatus = MessageFormatUtil.bytesToNumber(MessageFormatUtil.subByteArray(machineBytes, 0, 4), 4);
		
		machineMaintenceRequestDTO.setMaintenceStatus(maintenceStatus);
		return machineMaintenceRequestDTO;
	}
	public static byte[] packetDtoToMachineBytes(MachineMaintenceResponseDTO machineMaintenceResponseDTO, Long messageId){
		char[] appendChars = new char[0];
		WarehousePacketModel warehousePacketModel = null;
		if(machineMaintenceResponseDTO == null){
			
			warehousePacketModel = new WarehousePacketModel(WarehousePacketEnum.MAINTAIN_RESPONSE.getFunctionCode().longValue(), messageId + 1, appendChars);
		}else{
			char[] returnValue = new char[0];
			Integer responseReturnValue = machineMaintenceResponseDTO.getReturnValue();
			
			returnValue = MessageFormatUtil.numberToChars(responseReturnValue, 1);
			warehousePacketModel = new WarehousePacketModel(WarehousePacketEnum.MAINTAIN_RESPONSE.getFunctionCode().longValue(), messageId + 1, returnValue);
		}
		return warehousePacketModel.getBytes(LinkInteractionEnum.ECHO_RESPONSE);
	}
}
