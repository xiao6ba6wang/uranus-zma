package com.empress.uranus.zma.biz.machine.adapter;

import com.empress.uranus.zma.common.enums.LinkInteractionEnum;
import com.empress.uranus.zma.common.enums.WarehousePacketEnum;
import com.empress.uranus.zma.common.utils.MessageFormatUtil;
import com.empress.uranus.zma.domain.machine.dto.request.MachineReviseVitualRequestDTO;
import com.empress.uranus.zma.domain.machine.dto.response.MachineReviseVitualResponseDTO;
import com.empress.uranus.zma.service.model.WarehousePacketModel;

public class MachineReviseAdapter {

	public static MachineReviseVitualRequestDTO machineBytesToPacketDTO(byte[] machineBytes){
		
		MachineReviseVitualRequestDTO machineReviseVitualRequestDTO = new MachineReviseVitualRequestDTO();
		Integer virtualCode = MessageFormatUtil.bytesToNumber(MessageFormatUtil.subByteArray(machineBytes, 0, 4), 4);
		
		machineReviseVitualRequestDTO.setVirtualCode(virtualCode);
		return machineReviseVitualRequestDTO;
	}
	public static byte[] packetDtoToMachineBytes(MachineReviseVitualResponseDTO machineReviseVitualResponseDTO, Long messageId){
		char[] appendChars = new char[0];
		WarehousePacketModel warehousePacketModel = null;
		if(machineReviseVitualResponseDTO == null){
			
			warehousePacketModel = new WarehousePacketModel(WarehousePacketEnum.REVISE_VIRTUAL_RESPONSE.getFunctionCode().longValue(), messageId + 1, appendChars);
		}else{
			char[] returnValue = new char[0];
			Integer responseReturnValue = machineReviseVitualResponseDTO.getReturnValue();
			
			returnValue = MessageFormatUtil.numberToChars(responseReturnValue, 1);
			warehousePacketModel = new WarehousePacketModel(WarehousePacketEnum.REVISE_VIRTUAL_RESPONSE.getFunctionCode().longValue(), messageId + 1, returnValue);
		}
		return warehousePacketModel.getBytes(LinkInteractionEnum.ECHO_RESPONSE);
	}
}
