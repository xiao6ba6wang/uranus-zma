package com.empress.uranus.zma.biz.impl;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.empress.uranus.zma.biz.INapkinBiz;
import com.empress.uranus.zma.service.INapkinService;

/**
 * 业务层接口实现类
 */
@Service("napkinBiz")
public class NapkinBizImpl implements INapkinBiz {

    private static final Logger LOGGER = LoggerFactory.getLogger(NapkinBizImpl.class);

	@Resource(name="napkinService")
    private INapkinService napkinService;
	
	@Override
	public void sellNapkinProduct() {
		
		LOGGER.info("do some work");
		napkinService.getNapkinInfo();
	}

}