package com.empress.uranus.zma.biz;

/**
 * 业务处理层
 * 	接口类
 */
public interface INapkinBiz {

    /**
	 * 定义接口
     */
    void sellNapkinProduct();
}