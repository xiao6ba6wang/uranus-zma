package com.empress.uranus.zma.biz.machine.adapter;

import com.empress.uranus.zma.common.enums.LinkInteractionEnum;
import com.empress.uranus.zma.common.enums.WarehousePacketEnum;
import com.empress.uranus.zma.common.utils.MessageFormatUtil;
import com.empress.uranus.zma.domain.machine.dto.request.MachineChargeRequestDTO;
import com.empress.uranus.zma.domain.machine.dto.response.MachineChargeResponseDTO;
import com.empress.uranus.zma.service.model.WarehousePacketModel;

public class MachineChargeAdapter {

	public static MachineChargeRequestDTO machineBytesToPacketDTO(byte[] machineBytes){
		
		MachineChargeRequestDTO machineChargeRequestDTO = new MachineChargeRequestDTO();
		Integer usbPort = MessageFormatUtil.bytesToNumber(MessageFormatUtil.subByteArray(machineBytes, 0, 4), 4);
		Integer usbCount = MessageFormatUtil.bytesToNumber(MessageFormatUtil.subByteArray(machineBytes, 4, 4), 4);
		
		machineChargeRequestDTO.setUsbPort(usbPort);
		machineChargeRequestDTO.setUsbCount(usbCount);
		return machineChargeRequestDTO;
	}
	public static byte[] packetDtoToMachineBytes(MachineChargeResponseDTO machineChargeResponseDTO, Long messageId){
		char[] appendChars = new char[0];
		WarehousePacketModel warehousePacketModel = null;
		if(machineChargeResponseDTO == null){
			
			warehousePacketModel = new WarehousePacketModel(WarehousePacketEnum.CHARGE_RESPONSE.getFunctionCode().longValue(), messageId + 1, appendChars);
		}else{
			char[] returnValue = new char[0];
			Integer responseReturnValue = machineChargeResponseDTO.getReturnValue();
			
			returnValue = MessageFormatUtil.numberToChars(responseReturnValue, 1);
			warehousePacketModel = new WarehousePacketModel(WarehousePacketEnum.CHARGE_RESPONSE.getFunctionCode().longValue(), messageId + 1, returnValue);
		}
		return warehousePacketModel.getBytes(LinkInteractionEnum.ECHO_RESPONSE);
	}
}