package com.empress.uranus.zma.biz.machine.adapter;

import com.empress.uranus.zma.common.enums.LinkInteractionEnum;
import com.empress.uranus.zma.common.enums.WarehousePacketEnum;
import com.empress.uranus.zma.common.utils.MessageFormatUtil;
import com.empress.uranus.zma.domain.machine.dto.request.MachineHeartBeatRequestDTO;
import com.empress.uranus.zma.domain.machine.dto.response.MachineHeartBeatResponseDTO;
import com.empress.uranus.zma.service.model.WarehousePacketModel;

public class MachineHeartBeatAdapter {

	public static MachineHeartBeatRequestDTO machineBytesToPacketDTO(byte[] machineBytes){
		
		MachineHeartBeatRequestDTO machineHeartBeatRequestDTO = new MachineHeartBeatRequestDTO();
		Integer operateStatus = MessageFormatUtil.bytesToNumber(MessageFormatUtil.subByteArray(machineBytes, 0, 4), 4);
		
		machineHeartBeatRequestDTO.setOperateStatus((long)operateStatus);
		return machineHeartBeatRequestDTO;
	}
	public static byte[] packetDtoToMachineBytes(MachineHeartBeatResponseDTO machineHeartBeatResponseDTO, Long messageId){
		char[] appendChars = new char[0];
		WarehousePacketModel warehousePacketModel = null;
		if(machineHeartBeatResponseDTO == null){
			
			warehousePacketModel = new WarehousePacketModel(WarehousePacketEnum.HEARTBEAT_RESPONSE.getFunctionCode().longValue(), messageId + 1, appendChars);
		}else{
			char[] returnValue = new char[0];
			Integer responseReturnValue = machineHeartBeatResponseDTO.getReturnValue();
			
			returnValue = MessageFormatUtil.numberToChars(responseReturnValue, 1);
			warehousePacketModel = new WarehousePacketModel(WarehousePacketEnum.HEARTBEAT_RESPONSE.getFunctionCode().longValue(), messageId + 1, returnValue);
		}
		return warehousePacketModel.getBytes(LinkInteractionEnum.ECHO_RESPONSE);
	}
}
