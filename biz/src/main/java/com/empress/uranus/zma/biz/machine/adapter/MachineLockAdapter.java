package com.empress.uranus.zma.biz.machine.adapter;

import com.empress.uranus.zma.common.enums.LinkInteractionEnum;
import com.empress.uranus.zma.common.enums.WarehousePacketEnum;
import com.empress.uranus.zma.common.utils.MessageFormatUtil;
import com.empress.uranus.zma.domain.machine.dto.request.MachineLockRequestDTO;
import com.empress.uranus.zma.domain.machine.dto.response.MachineLockResponseDTO;
import com.empress.uranus.zma.service.model.WarehousePacketModel;

public class MachineLockAdapter {

	public static MachineLockRequestDTO machineBytesToPacketDTO(byte[] machineBytes){
		
		MachineLockRequestDTO machineLockRequestDTO = new MachineLockRequestDTO();
		Integer controlCode = MessageFormatUtil.bytesToNumber(MessageFormatUtil.subByteArray(machineBytes, 0, 4), 4);
		
		machineLockRequestDTO.setControlCode(controlCode);
		return machineLockRequestDTO;
	}
	public static byte[] packetDtoToMachineBytes(MachineLockResponseDTO machineLockResponseDTO, Long messageId){
		char[] appendChars = new char[0];
		WarehousePacketModel warehousePacketModel = null;
		if(machineLockResponseDTO == null){
			
			warehousePacketModel = new WarehousePacketModel(WarehousePacketEnum.ELECTROMAGNETIC_LOCK_RESPONSE.getFunctionCode().longValue(), messageId + 1, appendChars);
		}else{
			char[] returnValue = new char[0];
			Integer responseReturnValue = machineLockResponseDTO.getReturnValue();
			
			returnValue = MessageFormatUtil.numberToChars(responseReturnValue, 1);
			warehousePacketModel = new WarehousePacketModel(WarehousePacketEnum.ELECTROMAGNETIC_LOCK_RESPONSE.getFunctionCode().longValue(), messageId + 1, returnValue);
		}
		return warehousePacketModel.getBytes(LinkInteractionEnum.ECHO_RESPONSE);
	}
}
