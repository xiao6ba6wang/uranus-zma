package com.empress.uranus.zma.biz.machine.adapter;

import com.empress.uranus.zma.common.enums.LinkInteractionEnum;
import com.empress.uranus.zma.common.enums.WarehousePacketEnum;
import com.empress.uranus.zma.common.utils.MessageFormatUtil;
import com.empress.uranus.zma.domain.machine.dto.request.MachineRegisterRequestDTO;
import com.empress.uranus.zma.domain.machine.dto.response.MachineRegisterResponseDTO;
import com.empress.uranus.zma.service.model.WarehousePacketModel;

public class MachineRegisterAdapter {
	
	public static MachineRegisterRequestDTO machineBytesToPacketDTO(byte[] machineBytes){
		
		MachineRegisterRequestDTO machineRegisterDTO = new MachineRegisterRequestDTO();
		String circuitBoard = MessageFormatUtil.bytesToString(MessageFormatUtil.subByteArray(machineBytes, 0, 33));
		
		machineRegisterDTO.setCircuitBoard(circuitBoard.trim());
		return machineRegisterDTO;
	}
	public static byte[] packetDtoToMachineBytes(MachineRegisterResponseDTO machineRegisterResponseDTO, Long messageId){
		char[] appendChars = new char[0];
		WarehousePacketModel warehousePacketModel = null;
		if(machineRegisterResponseDTO == null){
			
			warehousePacketModel = new WarehousePacketModel(WarehousePacketEnum.REGISTER_RESPONSE.getFunctionCode().longValue(), messageId + 1, appendChars);
		}else{
			char[] returnValue = new char[0];
			Integer responseReturnValue = machineRegisterResponseDTO.getReturnValue();
			
			returnValue = MessageFormatUtil.numberToChars(responseReturnValue, 1);
			warehousePacketModel = new WarehousePacketModel(WarehousePacketEnum.REGISTER_RESPONSE.getFunctionCode().longValue(), messageId + 1, returnValue);
		}
		return warehousePacketModel.getBytes(LinkInteractionEnum.ECHO_RESPONSE);
	}
}



