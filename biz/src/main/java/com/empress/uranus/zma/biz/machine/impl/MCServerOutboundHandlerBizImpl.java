package com.empress.uranus.zma.biz.machine.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.empress.uranus.zma.biz.machine.MCServerOutboundHandlerBiz;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;

/**
 * 纸巾机服务器Server层处理数据输出实现类
 * 
 * @author taoxuejun
 * @date 2017-12-10
 */
@ChannelHandler.Sharable
@Service("mcserverOutboundHandlerBiz")
public class MCServerOutboundHandlerBizImpl  extends MCServerOutboundHandlerBiz {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MCServerOutboundHandlerBizImpl.class);

	@Override
	public void write(ChannelHandlerContext ctx, Object sendMessage, ChannelPromise promise) throws Exception {
		
		LOGGER.info("the napkin machine server response outbound handler channel write: ctx :" + ctx);
		//取出业务端的响应数据，并将其对象转换成字节数组
    	ByteBuf responseBytesBuffer = (ByteBuf)sendMessage;
    	int responseBytesBufferLength = responseBytesBuffer.readableBytes();
    	byte[] responseBytes = new byte[responseBytesBufferLength];
    	responseBytesBuffer.readBytes(responseBytes);
    	ByteBuf encoded = ctx.alloc().buffer(responseBytesBufferLength);
    	encoded.writeBytes(responseBytes);
    	ctx.write(encoded);
    	ctx.flush();
	}
}
