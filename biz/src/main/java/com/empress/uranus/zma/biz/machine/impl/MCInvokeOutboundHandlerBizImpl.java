package com.empress.uranus.zma.biz.machine.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.empress.uranus.zma.biz.machine.MCInvokeOutboundHandlerBiz;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;

/**
 * 纸巾机服务器业务层处理数据输出实现类
 * 
 * @author taoxuejun
 * @date 2017-12-10
 */
@ChannelHandler.Sharable
@Service("mcinvokeOutboundHandlerBiz")
public class MCInvokeOutboundHandlerBizImpl extends MCInvokeOutboundHandlerBiz {

	private static final Logger LOGGER = LoggerFactory.getLogger(MCInvokeOutboundHandlerBizImpl.class);

	@Override
	public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
		
		LOGGER.info("the napkin machine server invoke outbound handler channel write: ctx :" + ctx);
		// 执行下一个OutboundHandler
		super.write(ctx, msg, promise);
	}
}
