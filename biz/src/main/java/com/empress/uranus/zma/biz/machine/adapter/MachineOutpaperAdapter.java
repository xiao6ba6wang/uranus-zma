package com.empress.uranus.zma.biz.machine.adapter;

import com.empress.uranus.zma.common.enums.LinkInteractionEnum;
import com.empress.uranus.zma.common.enums.WarehousePacketEnum;
import com.empress.uranus.zma.common.utils.MessageFormatUtil;
import com.empress.uranus.zma.domain.machine.dto.request.MachineOutpaperRequestDTO;
import com.empress.uranus.zma.domain.machine.dto.response.MachineOutpaperResponseDTO;
import com.empress.uranus.zma.service.model.WarehousePacketModel;

public class MachineOutpaperAdapter {

	public static MachineOutpaperRequestDTO machineBytesToPacketDTO(byte[] machineBytes){
		
		MachineOutpaperRequestDTO machineOutpaperRequestDTO = new MachineOutpaperRequestDTO();
		Integer outpaperStatus = MessageFormatUtil.bytesToNumber(MessageFormatUtil.subByteArray(machineBytes, 0, 4), 4);
		
		machineOutpaperRequestDTO.setOutpaperStatus(outpaperStatus);
		return machineOutpaperRequestDTO;
	}
	public static byte[] packetDtoToMachineBytes(MachineOutpaperResponseDTO machineOutpaperResponseDTO, Long messageId){
		char[] appendChars = new char[0];
		WarehousePacketModel warehousePacketModel = null;
		if(machineOutpaperResponseDTO == null){
			
			warehousePacketModel = new WarehousePacketModel(WarehousePacketEnum.OUTPAPER_FEEDBACK_RESPONSE.getFunctionCode().longValue(), messageId + 1, appendChars);
		}else{
			char[] returnValue = new char[0];
			Integer responseReturnValue = machineOutpaperResponseDTO.getReturnValue();
			
			returnValue = MessageFormatUtil.numberToChars(responseReturnValue, 1);
			warehousePacketModel = new WarehousePacketModel(WarehousePacketEnum.OUTPAPER_FEEDBACK_RESPONSE.getFunctionCode().longValue(), messageId + 1, returnValue);
		}
		return warehousePacketModel.getBytes(LinkInteractionEnum.ECHO_RESPONSE);
	}
}
