package com.empress.uranus.zma.biz.machine.adapter;

import com.empress.uranus.zma.common.enums.LinkInteractionEnum;
import com.empress.uranus.zma.common.enums.WarehousePacketEnum;
import com.empress.uranus.zma.common.utils.MessageFormatUtil;
import com.empress.uranus.zma.domain.machine.dto.request.MachineHeartBeatIntervalRequestDTO;
import com.empress.uranus.zma.domain.machine.dto.response.MachineHeartBeatIntervalResponseDTO;
import com.empress.uranus.zma.service.model.WarehousePacketModel;

public class MachineHeartBeatIntervalAdapter {

	public static MachineHeartBeatIntervalRequestDTO machineBytesToPacketDTO(byte[] machineBytes){
		
		MachineHeartBeatIntervalRequestDTO machineHeartBeatIntervalRequestDTO = new MachineHeartBeatIntervalRequestDTO();
		Long heartBeatInterval = (long)MessageFormatUtil.bytesToNumber(MessageFormatUtil.subByteArray(machineBytes, 0, 4), 4);
		
		machineHeartBeatIntervalRequestDTO.setHeartBeatInterval(heartBeatInterval);
		return machineHeartBeatIntervalRequestDTO;
	}
	public static byte[] packetDtoToMachineBytes(MachineHeartBeatIntervalResponseDTO machineHeartBeatIntervalResponseDTO, Long messageId){
		char[] appendChars = new char[0];
		WarehousePacketModel warehousePacketModel = null;
		if(machineHeartBeatIntervalResponseDTO == null){
			
			warehousePacketModel = new WarehousePacketModel(WarehousePacketEnum.HEARTBEAT_INTERVAL_RESPONSE.getFunctionCode().longValue(), messageId + 1, appendChars);
		}else{
			char[] returnValue = new char[0];
			Integer responseReturnValue = machineHeartBeatIntervalResponseDTO.getReturnValue();
			
			returnValue = MessageFormatUtil.numberToChars(responseReturnValue, 1);
			warehousePacketModel = new WarehousePacketModel(WarehousePacketEnum.HEARTBEAT_INTERVAL_RESPONSE.getFunctionCode().longValue(), messageId + 1, returnValue);
		}
		return warehousePacketModel.getBytes(LinkInteractionEnum.ECHO_RESPONSE);
	}
}
