package com.empress.uranus.zma.biz.machine.adapter;

import com.empress.uranus.zma.common.enums.LinkInteractionEnum;
import com.empress.uranus.zma.common.enums.WarehousePacketEnum;
import com.empress.uranus.zma.common.utils.MessageFormatUtil;
import com.empress.uranus.zma.domain.machine.dto.request.MachineShipmentRequestDTO;
import com.empress.uranus.zma.domain.machine.dto.response.MachineShipmentResponseDTO;
import com.empress.uranus.zma.service.model.WarehousePacketModel;

public class MachineShipmentAdapter {

	public static MachineShipmentRequestDTO machineBytesToPacketDTO(byte[] machineBytes){
		
		MachineShipmentRequestDTO machineShipmentRequestDTO = new MachineShipmentRequestDTO();
		Integer shipCode = MessageFormatUtil.bytesToNumber(MessageFormatUtil.subByteArray(machineBytes, 0, 4), 4);
		
		machineShipmentRequestDTO.setShipCode(shipCode);
		return machineShipmentRequestDTO;
	}
	public static byte[] packetDtoToMachineBytes(MachineShipmentResponseDTO machineShipmentResponseDTO, Long messageId){
		char[] appendChars = new char[0];
		WarehousePacketModel warehousePacketModel = null;
		if(machineShipmentResponseDTO == null){
			
			warehousePacketModel = new WarehousePacketModel(WarehousePacketEnum.SHIPMENT_RESPONSE.getFunctionCode().longValue(), messageId + 1, appendChars);
		}else{
			char[] returnValue = new char[0];
			Integer responseReturnValue = machineShipmentResponseDTO.getReturnValue();
			
			returnValue = MessageFormatUtil.numberToChars(responseReturnValue, 1);
			warehousePacketModel = new WarehousePacketModel(WarehousePacketEnum.SHIPMENT_RESPONSE.getFunctionCode().longValue(), messageId + 1, returnValue);
		}
		return warehousePacketModel.getBytes(LinkInteractionEnum.ECHO_RESPONSE);
	}
}
