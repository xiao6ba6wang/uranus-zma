package com.empress.uranus.zma.biz.machine;

import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * 纸巾机服务器心跳接口
 * 
 * @author taoxuejun
 * @date 2017-12-10
 */
public abstract class HeartbeatServerHandlerBiz extends ChannelInboundHandlerAdapter {

}
