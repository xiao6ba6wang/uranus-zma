#!/usr/bin/env bash
# ------------------------------------
# default jvm args if you do not config in /jetty/boot.ini
# ------------------------------------
JVM_ARGS="-server -Dfile.encoding=UTF-8 -Dsun.jnu.encoding=UTF-8 -Djava.io.tmpdir=/tmp -Djava.net.preferIPv6Addresses=false -Ddubbo.application.logger=slf4j"
JVM_GC="-XX:+DisableExplicitGC -XX:+PrintGCDetails -XX:+PrintHeapAtGC -XX:+PrintTenuringDistribution -XX:+UseConcMarkSweepGC -XX:+PrintGCTimeStamps -XX:+PrintGCDateStamps"
JVM_GC=$JVM_GC" -XX:CMSFullGCsBeforeCompaction=0 -XX:+UseCMSCompactAtFullCollection -XX:CMSInitiatingOccupancyFraction=80"
JVM_HEAP="-XX:SurvivorRatio=8 -XX:MetaspaceSize=256m -XX:MaxMetaspaceSize=256m -XX:+HeapDumpOnOutOfMemoryError -XX:ReservedCodeCacheSize=128m -XX:InitialCodeCacheSize=128m"
JVM_SIZE="-Xmx$JVM_XMX -Xms$JVM_XMS"

# ------------------------------------
# do not edit
# ------------------------------------

function monitorinit(){
    unzip ${WEB_ROOT}/WEB-INF/lib/inf-zhanganchu-xcat-*.jar -d xcat-client
    if [ -z "$XML_DIR" ]; then
       XML_DIR="/opt/meituan/mobile/xcat-client"
    fi
    if [ ! -d "$XML_DIR" ]; then
       mkdir -p $XML_DIR
    fi

    if [ -z "$LOG_DIR" ]; then
        LOG_DIR="/opt/logs/mobile/xcat-client"
    fi
    if [ ! -d "$LOG_DIR" ]; then
       mkdir -p $LOG_DIR
    fi

    XML_FILE="${XML_DIR}/client.xml"
    SRC_XML_FILE="xcat-client/config/${ENV}-client.xml"
    if [ -s $SRC_XML_FILE ]; then
        cp xcat-client/config/${ENV}-client.xml $XML_FILE
    else
        echo "WARN: ${ENV}-client.xml doesnot exist."
    fi
}



function init() {
    if [ -z "$LOG_PATH" ]; then
        LOG_PATH="/opt/logs/mobile/$MODULE"
    fi

    if [ -z "$WORK_PATH" ]; then
        WORK_PATH="/opt/meituan/mobile/$MODULE"
    fi
    WEB_ROOT=$WORK_PATH/webroot
    unzip *.war -d webroot
    mkdir -p $LOG_PATH

    monitorinit

    #定时清理日志
    cleanpath="$WORK_PATH/clean.sh"
    echo "#!/bin/bash" > $cleanpath
    echo "find $LOG_PATH -mtime +1 -exec /bin/gzip {} \;" >> $cleanpath
    echo "find $LOG_PATH -mtime +3 -exec rm -fr {} \;" >> $cleanpath
    chmod +x $cleanpath
    (crontab -l|grep -v $cleanpath ; echo "58 05 * * * /bin/bash $cleanpath > /dev/null 2>&1" ) | crontab
}

function run() {
    #根据java版本,决定java命令的位置
    JAVA_CMD=$JAVA_VERSION
    if [ -z "$JAVA_VERSION" ]; then
        JAVA_CMD="java" #系统默认的java命令
    else
        JAVA_CMD="/usr/local/$JAVA_VERSION/bin/java"
    fi

    EXEC="exec"
    CONTEXT=/
    cd $WEB_ROOT
    if [ -e "WEB-INF/classes/release" ]; then
        cp -rf WEB-INF/classes/release/* WEB-INF/classes
    fi
    if [ -e "WEB-INF/classes/jetty/boot.ini" ]; then
        source WEB-INF/classes/jetty/boot.ini
    fi


    CLASSPATH=WEB-INF/classes
    for i in WEB-INF/lib/*
    do
        CLASSPATH=$CLASSPATH:$i
    done
    export CLASSPATH
    JAVA_ARGS="-Djetty.webroot=$WEB_ROOT"
    EXEC_JAVA="$EXEC $JAVA_CMD $JVM_ARGS $JVM_SIZE $JVM_HEAP $JVM_JIT $JVM_GC"
    EXEC_JAVA=$EXEC_JAVA" -Xloggc:$LOG_PATH/$MODULE.gc.log -XX:ErrorFile=$LOG_PATH/$MODULE.vmerr.log -XX:HeapDumpPath=$LOG_PATH/$MODULE.heaperr.log"
    EXEC_JAVA=$EXEC_JAVA" -Djetty.appkey=$MODULE -Djetty.context=$CONTEXT -Djetty.logs=$LOG_PATH"
    EXEC_JAVA=$EXEC_JAVA" $JAVA_ARGS"
    if [ "$UID" = "0" ]; then
        ulimit -n 1024000
        umask 000
    else
        echo $EXEC_JAVA
    fi
    $EXEC_JAVA com.empress.uranus.zma.web.AppServiceApplication > /dev/null 2>&1
}

# ------------------------------------
# actually work
# ------------------------------------
init
run