<!DOCTYPE html>
<html>
<script src='lib/jquery-3.2.1.min.js' type='text/javascript'></script>
<script>
    function call() {
        var suc = {"success": true};
        //var suc = "<@=json@>";
        //console.log(suc);
        var emt = {"filmList": {}};
        var req = {
            "requestId": $("#requestId").val(),
            "data": JSON.stringify(suc),
            "code": 100000,
            "type": "SU",
            "message": "suc"
        };
        var flag = $("#requestId").val();
        var screen = {"cinemaCode":"00000000","screenCode":"0000000000000034"};
        var seat = {"cinemaCode":"00000000","screenCode":"0000000000000034"};
        //var seat = screen;
        //var rawdata = {"cinemaCode":"1111","startBusinessDate":"2017-01-01","endBusinessDate":"2017-07-07","screenCode":"222","filmCode":"12323","sessionCode":"set22","code":"12323","startDatetime":"2017-01-01T00:00:00","endDatetime":"2017-01-01T12:21:50","flag":0};
        var rawdata = {"cinemaCode":"1111"};

        var notice = {"cinemaCode":"2222", "code":1111, "title":"test", "source":"test", "contentLength":4, "content":"test"};
        var comnot = {"cinemaCode":"2222","dataBody":{"code":1111, "title":"test", "source":"test", "contentLength":4, "content":"test"}};

        var schedule = {"cinemaCode" : "222", "dataBody" : "{\"startBusinessDate\":\"2016-01-01\",\"endBusinessDate\":\"2018-01-01\",\"screenCode\":\"0000000000000034\",\"filmCode\":\"001352344689\",\"sessionCode\":\"0000000000000011\",\"startSessionDate\":\"2016-01-01T01:01:01\",\"endSessionDate\":\"2018-01-01T01:01:01\"}"};

        var testreq = {"pa1":"1","pa3":2,"obj":"{\"cinemaCode\":\"1111\",\"code\":2222,\"title\":\"wer\",\"source\":\"wetset\",\"contentLength\":1,\"content\":\"sfsdfsdfsdf\"}"};
        var screenurl = "/client/queryscreeninfo";
        var seaturl = "/client/queryscreenseatinfo";
        var rawdataurl = "/client/queryrawdata";
        var noticeurl = "/client/notice";
        var testurl = "/client/test";
        var  scheduleurl = "/req/queryschedule";
        var logreq = {"cinemaCode" : "41010101", "dataBody" : "{\"startDatetime\":\"2017-05-26T01:01:01\", \"endDatetime\":\"2017-09-30T01:01:01\"}"};
        var logurl = "/client/querycinemalog";

        var authreq = {"cinemaCode" : "41010101", "dataBody" : ""};
        var authurl = "/client/querycinemaauthinfo";

        var statistic = 1;
        var statisticurl = "/statisticreport/dostatisticreport";
        var url;
        if(flag == 0){
            req = screen;
            url = screenurl;
        } else if(flag == 1) {
            req = seat;
            url = seaturl;
        } else if(flag == 2) {
            req = rawdata;
            url = rawdataurl
        } else if(flag == 3) {
            req = notice;
            url = noticeurl;
        } else if(flag == 4) {
            req = schedule;
            url = scheduleurl;
        } else if(flag == 5) {
            req = logreq;
            url = logurl;
        } else if(flag == 6) {
            req = authreq;
            url = authurl;
        }
        else if(flag == 7) {
            //req = statistic;
            url = statisticurl;
        } else if(flag == 1000){
            req = testreq;
            url = testurl;
        } else if(flag == 101) {
            req = comnot;
            url = noticeurl;
        }

        $.ajax({
            type: "POST",  //提交方式
            url: url,//路径
            dataType: "json",
            contentType: "application/json",
            data:JSON.stringify(req),
                //数据，这里使用的是Json格式进行传输
            success: function (result) {//返回数据根据结果进行相应的处理
                console.log(result);
                alert("success");
            }
        });
    }

</script>
<head>
    <title>Title</title>
</head>
<body>
<span>RequestID  </span><input type="text" id="requestId"/>
<a href="#" onclick="call()">callback</a>
</body>
</html>
