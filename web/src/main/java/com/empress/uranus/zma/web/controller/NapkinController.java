/**
 * @(#)ControllerExample.java, 2017-2-4. Copyright 2017 Meituan, Inc. All rights
 * reserved. MEITUAN PROPRIETARY/CONFIDENTIAL. Use
 * is subject to license terms.
 */
package com.empress.uranus.zma.web.controller;

import com.alibaba.fastjson.JSONObject;
import com.empress.uranus.zma.biz.INapkinBiz;
import com.empress.uranus.zma.domain.vo.request.NapkinRequest;
import com.empress.uranus.zma.domain.vo.response.NapkinResponse;
import com.wordnik.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 这是直接用了Autowired spring xml功能 <br>
 * 其中@ApiOperation和@ApiParam为添加的API相关注解，个参数说明如下：<br>
 * At ApiOperation(value = “接口说明”, httpMethod = “接口请求方式”, response = “接口返回参数类型”,
 * notes = “接口发布说明”；其他参数可参考源码；<br>
 * At ApiParam(required = “是否必须参数”, name = “参数名称”, value = “参数具体描述”
 */
@SpringBootApplication
@RestController
@RequestMapping("/exp")
public class NapkinController {

    private static final Logger LOGGER = LoggerFactory.getLogger(NapkinController.class);

    @Resource(name="napkinBiz")
    private INapkinBiz napkinBiz;

    @ApiOperation(value = "querynapkininfo", httpMethod = "POST", response = Object.class, notes = "查询纸巾信息")
    @RequestMapping(value = "/querynapkininfo", method = RequestMethod.POST)
    public NapkinResponse queryScreenInfo(@RequestBody NapkinRequest napkinRequest) {
    	
    	LOGGER.info("前端请求参数{}", JSONObject.toJSON(napkinRequest));
        
    	napkinBiz.sellNapkinProduct();
    	LOGGER.info("后端响应体{}", JSONObject.toJSON(napkinRequest));
        return new NapkinResponse();
    }
    
    @ApiOperation(value = "test", httpMethod = "GET", response = Object.class, notes = "查询纸巾信息")
    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public NapkinResponse queryScreenInfo() {
    	
    	LOGGER.info("前端请求参数{}");
        
    	napkinBiz.sellNapkinProduct();
    	LOGGER.info("后端响应体{}");
        return new NapkinResponse();
    }
}
