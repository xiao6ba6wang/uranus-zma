package com.empress.uranus.zma.service.model;

import com.empress.uranus.zma.common.enums.LinkInteractionEnum;
import com.empress.uranus.zma.common.utils.MessageFormatUtil;
import com.empress.uranus.zma.domain.machine.packet.WarehousePacket;

public class WarehousePacketModel {

	private Long functionCode;
	private Long messageId;
	private char[] pdata;
	
	public WarehousePacketModel(Long functionCode, Long messageId, char[] pdata) {
        this.functionCode = functionCode;
        this.messageId = messageId;
        this.pdata = pdata;
    }
    private char[] getCharArray(int packetType) {
        char[] data = new char[18 + pdata.length];
        //1请求体 2响应体
        if(packetType == 1){
        	
        	data[0] = 0x3E;
            data[1] = 0x2A;
        } else {
        	
        	data[0] = 0x3C;
            data[1] = 0x2A;
        }
        //整个包长度
        int lowPacketLength = data.length % (256*256);
        int highPacketLength = data.length / (256*256);
        data[2] = (char) (highPacketLength / 256);
        data[3] = (char) (highPacketLength % 256);
        data[4] = (char) (lowPacketLength / 256);
        data[5] = (char) (lowPacketLength % 256);
        //消息头长度
        data[6] = 0x00;
        data[7] = 0x0A;
        //消息版本
        data[8] = 0x00;
        data[9] = 0x01;
        //功能码
        long lowFunctionCode = functionCode % (256*256);
        long highFunctionCode = functionCode / (256*256);
        data[10] = (char) (highFunctionCode / 256);
        data[11] = (char) (highFunctionCode % 256);
        data[12] = (char) (lowFunctionCode / 256);
        data[13] = (char) (lowFunctionCode % 256);
        //消息序号
        long lowMessageId = messageId % (256*256);
        long highMessageId = messageId / (256*256);
        data[14] = (char) (highMessageId / 256);
        data[15] = (char) (highMessageId % 256);
        data[16] = (char) (lowMessageId / 256);
        data[17] = (char) (lowMessageId % 256);
        //消息体
        for (int i = 0; i < pdata.length; i++) {
            data[i + 18] = pdata[i];
        }
        return data;
    }	
	public byte[] getBytes(LinkInteractionEnum linkInteractionEnum) {
        char[] chars = getCharArray(linkInteractionEnum.getPacketType().intValue());
        byte[] bytes = new byte[chars.length];
        int i=0;
        while(i<chars.length) {
            bytes[i] = (byte)chars[i];
            i++;
        }
        return bytes;
    }
	
    public static WarehousePacket buildWarehousePacketModel(byte[] packetBytes){
    	
    	WarehousePacket warehousePacket = new WarehousePacket();
    	if(packetBytes.length < 8){
    		
    		return warehousePacket;
    	}
    	for(int i=0; i<packetBytes.length; i++){
    		if(packetBytes[i] == 0x20){
    			packetBytes[i] = 0x00;
    		}
    	}
    	Integer syncStart = MessageFormatUtil.bytesToNumber(MessageFormatUtil.subByteArray(packetBytes, 0, 2), 2);
    	warehousePacket.setSyncStart(syncStart);
    	Integer packetLength = MessageFormatUtil.bytesToNumber(MessageFormatUtil.subByteArray(packetBytes, 2, 4), 4);
    	warehousePacket.setPacketLength(packetLength);
    	Integer headerLength = MessageFormatUtil.bytesToNumber(MessageFormatUtil.subByteArray(packetBytes, 6, 2), 2);
    	warehousePacket.setHeaderLength(headerLength);
    	Integer version = MessageFormatUtil.bytesToNumber(MessageFormatUtil.subByteArray(packetBytes, 8, 2), 2);
    	warehousePacket.setVersion(version);
    	Long functionCode = (long)MessageFormatUtil.bytesToNumber(MessageFormatUtil.subByteArray(packetBytes, 10, 4), 4);
    	warehousePacket.setFunctionCode(functionCode);
    	Long messageId = (long)MessageFormatUtil.bytesToNumber(MessageFormatUtil.subByteArray(packetBytes, 14, 4), 4);
    	warehousePacket.setMessageId(messageId);
    	
    	byte[] payloadData = MessageFormatUtil.subByteArray(packetBytes, 18, packetBytes.length-18);
    	warehousePacket.setPayloadData(payloadData);
    	return warehousePacket;
    }
}
