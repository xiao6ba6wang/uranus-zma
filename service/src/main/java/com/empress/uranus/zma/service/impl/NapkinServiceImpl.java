package com.empress.uranus.zma.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.empress.uranus.zma.service.INapkinService;

/**
 * 服务层实现类
 */
@Service("napkinService")
public class NapkinServiceImpl implements INapkinService {

    private static final Logger LOGGER = LoggerFactory.getLogger(NapkinServiceImpl.class);

	@Override
	public void getNapkinInfo() {
		LOGGER.info("do some service work");
	}
}
