package com.empress.uranus.zma.dao;

import org.apache.ibatis.annotations.Select;

/**
 * 数据库接口层
 */
public interface INapkinDAO {

    @Select("SELECT napkin_order_info AS orderInfo  FROM napkin_order WHERE id= #{orderId}")
    String getNapkinInfoByOrderId(int orderId);
}
