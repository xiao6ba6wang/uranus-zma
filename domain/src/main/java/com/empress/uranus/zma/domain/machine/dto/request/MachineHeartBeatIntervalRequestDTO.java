package com.empress.uranus.zma.domain.machine.dto.request;

/**
 * 心跳间隔设置接口消息请求体
 * 
 * @author taoxuejun
 * @date 2017-12-12
 */
public class MachineHeartBeatIntervalRequestDTO {

	/**
	 * 心跳间隔设置
	 * 	单位秒
	 */
	private Long heartBeatInterval;
	public Long getHeartBeatInterval() {
		return heartBeatInterval;
	}
	public void setHeartBeatInterval(Long heartBeatInterval) {
		this.heartBeatInterval = heartBeatInterval;
	}
}
