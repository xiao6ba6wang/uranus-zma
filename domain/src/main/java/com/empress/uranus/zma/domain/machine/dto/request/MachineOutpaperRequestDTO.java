package com.empress.uranus.zma.domain.machine.dto.request;

/**
 * 出纸反馈接口消息请求体
 * 
 * @author taoxuejun
 * @date 2017-12-12
 */
public class MachineOutpaperRequestDTO {
	
	/**
	 * 出纸状态
	 */
	private Integer outpaperStatus;

	public Integer getOutpaperStatus() {
		return outpaperStatus;
	}
	public void setOutpaperStatus(Integer outpaperStatus) {
		this.outpaperStatus = outpaperStatus;
	}
}
