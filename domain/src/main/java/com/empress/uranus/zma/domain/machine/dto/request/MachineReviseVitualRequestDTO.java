package com.empress.uranus.zma.domain.machine.dto.request;

/**
 * 修改虚拟编号消息请求体
 * 
 * @author taoxuejun
 * @date 2017-12-12
 */
public class MachineReviseVitualRequestDTO {

	/**
	 * 修改虚拟编号号码
	 */
	private Integer virtualCode;
	public Integer getVirtualCode() {
		return virtualCode;
	}
	public void setVirtualCode(Integer virtualCode) {
		this.virtualCode = virtualCode;
	}
}
