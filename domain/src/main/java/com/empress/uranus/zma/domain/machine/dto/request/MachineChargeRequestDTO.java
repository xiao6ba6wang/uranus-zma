package com.empress.uranus.zma.domain.machine.dto.request;

/**
 * 充电接口消息请求体
 * 
 * @author taoxuejun
 * @date 2017-12-12
 */
public class MachineChargeRequestDTO {

	/**
	 * 后台指定usb口
	 */
	private Integer usbPort;
	/**
	 * 后台定义usb口个数
	 */
	private Integer usbCount;
	public Integer getUsbPort() {
		return usbPort;
	}
	public void setUsbPort(Integer usbPort) {
		this.usbPort = usbPort;
	}
	public Integer getUsbCount() {
		return usbCount;
	}
	public void setUsbCount(Integer usbCount) {
		this.usbCount = usbCount;
	}
}
