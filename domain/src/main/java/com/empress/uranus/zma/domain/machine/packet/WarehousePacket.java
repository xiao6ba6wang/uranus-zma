package com.empress.uranus.zma.domain.machine.packet;

public class WarehousePacket {

	private Integer syncStart;
	
	private Integer packetLength;
	
	private Integer headerLength;
	
	private Integer version;
	
	private Long functionCode;
	
	private Long messageId;
	
	private byte[] payloadData;
	
	private Integer syncEnd;

	public Integer getSyncStart() {
		return syncStart;
	}
	public void setSyncStart(Integer syncStart) {
		this.syncStart = syncStart;
	}
	public Integer getPacketLength() {
		return packetLength;
	}
	public void setPacketLength(Integer packetLength) {
		this.packetLength = packetLength;
	}
	public Integer getHeaderLength() {
		return headerLength;
	}
	public void setHeaderLength(Integer headerLength) {
		this.headerLength = headerLength;
	}
	public Integer getVersion() {
		return version;
	}
	public void setVersion(Integer version) {
		this.version = version;
	}
	public Long getFunctionCode() {
		return functionCode;
	}
	public void setFunctionCode(Long functionCode) {
		this.functionCode = functionCode;
	}
	public Long getMessageId() {
		return messageId;
	}
	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}
	public byte[] getPayloadData() {
		return payloadData;
	}
	public void setPayloadData(byte[] payloadData) {
		this.payloadData = payloadData;
	}
	public Integer getSyncEnd() {
		return syncEnd;
	}
	public void setSyncEnd(Integer syncEnd) {
		this.syncEnd = syncEnd;
	}
}
