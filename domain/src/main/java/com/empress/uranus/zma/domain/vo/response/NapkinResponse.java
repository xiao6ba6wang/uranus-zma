package com.empress.uranus.zma.domain.vo.response;

/**
 * 响应类
 */
public class NapkinResponse {

	/**
	 * 根据纸巾ID得到的纸巾信息字符串
	 */
	private String napkinInfo;
	public String getNapkinInfo() {
		return napkinInfo;
	}
	public void setNapkinInfo(String napkinInfo) {
		this.napkinInfo = napkinInfo;
	}
}
