package com.empress.uranus.zma.domain.machine.dto.request;

/**
 * 心跳接口消息请求体
 * 
 * @author taoxuejun
 * @date 2017-12-12
 */
public class MachineHeartBeatRequestDTO {

	/**
	 * 机器运营状态
	 */
	private Long operateStatus;
	public Long getOperateStatus() {
		return operateStatus;
	}
	public void setOperateStatus(Long operateStatus) {
		this.operateStatus = operateStatus;
	}
}
