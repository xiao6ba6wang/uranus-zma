package com.empress.uranus.zma.domain.machine.dto.request;

/**
 * 维护接口消息请求体
 * 
 * @author taoxuejun
 * @date 2017-12-12
 */
public class MachineMaintenceRequestDTO {

	/**
	 * 后台下发维护和取消维护状态
	 */
	private Integer maintenceStatus;
	public Integer getMaintenceStatus() {
		return maintenceStatus;
	}
	public void setMaintenceStatus(Integer maintenceStatus) {
		this.maintenceStatus = maintenceStatus;
	}
}
