package com.empress.uranus.zma.domain.machine.dto.request;

/**
 * 机器注册接口消息请求体
 * 
 * @author taoxuejun
 * @date 2017-12-12
 */
public class MachineRegisterRequestDTO {

	/**
	 * 虚拟字段   mock字段
	 * 	需要用户方定义
	 */
	private String circuitBoard;

	public String getCircuitBoard() {
		return circuitBoard;
	}
	public void setCircuitBoard(String circuitBoard) {
		this.circuitBoard = circuitBoard;
	}
}
