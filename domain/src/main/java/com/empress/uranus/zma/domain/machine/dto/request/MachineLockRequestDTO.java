package com.empress.uranus.zma.domain.machine.dto.request;

/**
 * 电磁锁接口消息请求体
 * 
 * @author taoxuejun
 * @date 2017-12-12
 */
public class MachineLockRequestDTO {

	/**
	 * 控制电磁锁的编号
	 */
	private Integer controlCode;
	public Integer getControlCode() {
		return controlCode;
	}
	public void setControlCode(Integer controlCode) {
		this.controlCode = controlCode;
	}
}
