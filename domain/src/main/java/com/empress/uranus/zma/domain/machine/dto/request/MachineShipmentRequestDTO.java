package com.empress.uranus.zma.domain.machine.dto.request;

/**
 * 出货接口消息请求体
 * 
 * @author taoxuejun
 * @date 2017-12-12
 */
public class MachineShipmentRequestDTO {

	/**
	 * 后台定义的出货口
	 */
	private Integer shipCode;
	public Integer getShipCode() {
		return shipCode;
	}
	public void setShipCode(Integer shipCode) {
		this.shipCode = shipCode;
	}
}
