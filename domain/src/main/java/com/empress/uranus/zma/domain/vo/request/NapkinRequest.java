/*
 * Copyright (c) 2017 empress.com
 * All rights reserved.
 *
 */
package com.empress.uranus.zma.domain.vo.request;

/**
 * 请求类
 */
public class NapkinRequest {

	/**
	 * 纸巾请求字段值
	 * 	纸巾ID
	 */
	private Integer napkinId;
	public Integer getNapkinId() {
		return napkinId;
	}
	public void setNapkinId(Integer napkinId) {
		this.napkinId = napkinId;
	}
}