package com.empress.uranus.zma.domain.machine.dto.response;

/**
 * 机器注册接口消息响应体
 * 
 * @author taoxuejun
 * @date 2017-12-12
 */
public class MachineRegisterResponseDTO {

	/**
	 * 虚拟字段   mock字段
	 * 	需要用户方定义
	 */
	private Integer returnValue;

	public Integer getReturnValue() {
		return returnValue;
	}
	public void setReturnValue(Integer returnValue) {
		this.returnValue = returnValue;
	}
}
